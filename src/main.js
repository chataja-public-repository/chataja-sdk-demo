import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import chataja from 'dev-chatta'

Vue.config.productionTip = false
Vue.prototype.$chataja = chataja;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
