import Vue from 'vue'
import Vuex from 'vuex'
import localstore from 'store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    roomList: [],
    createChatroom: [],
    activeRoom: {},
    roomMessages: [],
    forwardMessage: {},
    userList: [
      {
        avatar_url:"https://d1edrlpyc25xu0.cloudfront.net/image/upload/t5XWp2PBRt/1510641299-default_user_avatar.png",
        fullname:"Test User 141",
        phone_number:"+628681141",
        qiscus_email:"userid_36740_628681141@kiwari-stag.com",
        unique_user_id:null
      },
      {
        avatar_url:"https://file-manager-dev.chataja.co.id/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBcHRoIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--88445ecd2dcabf713eb433352666b90bf1ffbaf0/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9RWVhWMGIxOXZjbWxsYm5SVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--def8e34accccefe653bb0ef9582700349b085386/1610434799.png",
        fullname:"test user 115",
        phone_number:"+628681115",
        qiscus_email:"userid_36714_628681115@kiwari-stag.com",
        unique_user_id:null
      },
      {
        avatar_url:"https://d1edrlpyc25xu0.cloudfront.net/image/upload/t5XWp2PBRt/1510641299-default_user_avatar.png",
        fullname:"",
        phone_number:"+628681114",
        qiscus_email:"userid_36713_628681114@kiwari-stag.com",
        unique_user_id:null
      }
    ]
  },
  mutations: {
    SET_ROOM_LIST (state, payload) {
      state.roomList = payload.map((item) => ({
        ...item,
        isTyping: false,
        comments: []
      }))
    },
    SET_CREATE_CHATROOM (state, payload) {
      state.createChatroom = payload
    },
    SET_ROOM_MESSAGE (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === payload.room_id)[0]
      state.activeRoom = room
      room.comments = []
      room.comments = payload.messages
      room.comments = room.comments.sort((a, b) => {
        if (a.timestamp > b.timestamp) {
          return 1
        }
        if (a.timestamp < b.timestamp) {
          return -1
        }
      })
      state.roomMessages = room.comments
    },
    UPDATE_LAST_MESSAGE (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === payload.room_id)[0]
      const payloadTimestamp = new Date(payload.timestamp)
      if (room) {
        room.last_message = payload.message
        room.last_message_timestamps = payload.timestamp
        room.last_message_timestamps_int = payloadTimestamp.getTime() / 1000
      }
    },
    SET_NEW_MESSAGE (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === payload.room_id)[0]
      const payloadTimestamp = new Date(payload.timestamp)
      if (room?.comments.length > 0) {
        const filteredMessage = room.comments.filter(f => f.id === payload.id)
        const lastMessageTimestamp = new Date(room.last_message_timestamps)
        if (filteredMessage.length === 0) {
          if (payloadTimestamp >= lastMessageTimestamp) {
            room.last_message = payload.message
            room.last_message_timestamps = payload.timestamp
            room.last_message_timestamps_int = payloadTimestamp.getTime() / 1000
            room.unread_count = parseInt(room.unread_count) + 1
            room.comments.push(payload)
          } else {
            return
          }
        }
      } else {
        if (room) {
          room.last_message = payload.message
          room.last_message_timestamps = payloadTimestamp
          room.last_message_timestamps_int = payloadTimestamp.getTime() / 1000
          room.unread_count = parseInt(room.unread_count) + 1
          room.comments.push(payload)
        }
      }

      if (room.uniqueRoomId === state.activeRoom.uniqueRoomId) {
        state.roomMessages = room.comments
      }
    },
    ADD_SENT_MESSAGE (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === payload.room_id)[0]
      room.last_message = payload.message
      room.last_message_timestamps = payload.timestamp
      room.last_message_timestamps_int = payload.unix_timestamp
      room.comments.push(payload)
    },
    SET_MESSAGE_DELIVERED (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === payload.room_id)[0]
      const searchedComment = room.comments.filter(f => f.id === payload.id)[0]
      searchedComment.isDelivered = true
    },
    SET_MESSAGE_READ (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === payload.room_id)[0]
      const searchedComment = room.comments.filter(f => f.id === payload.id)[0]
      searchedComment.isRead = true
    },
    SET_TYPING_STATUS (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === parseInt(payload.room_id))[0]
      room.isTyping = parseInt(payload.message) === 1 ? true : false
    },
    CLEAR_ALL_MESSAGE (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === parseInt(payload))[0]
      room.comments = []
      state.roomMessages = room.comments
    },
    ADD_GROUP_ROOM (state, payload) {
      payload = {
        ...payload,
        isTyping: false,
        comments: [],
        last_message: '',
        last_message_timestamps: '',
        last_message_timestamps_int: 0
      }
      const room = state.roomList.filter(f => f.uniqueRoomId === parseInt(payload))[0]
      if (!room) {
        state.roomList.unshift(payload)
      }
    },
    ADD_GROUP_PARTICIPANT (state, payload) {
      const room = state.roomList.filter(f => f.uniqueRoomId === parseInt(payload.uniqueRoomId))[0]
      if (room) {
        room.users = payload.users
      }
    }
  },
  actions: {
  },
  getters: {
    getRoomList: state => state.roomList,
    getRoomMessages: state => state.roomMessages,
    getForwardMessage: state => state.forwardMessage,
    getUserList: state => state.userList,
    getActiveParticipant: state => state.activeRoom.users
  },
  modules: {
  }
})
